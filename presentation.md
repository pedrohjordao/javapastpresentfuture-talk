---
theme: gaia 
_class: lead
paginate: true
backgroundColor: #ffff
backgroundImage: url('https://marp.app/assets/hero-background.jpg')
footer: Mar 2020
marp: true
---

![left:30% 70%](assets/Duke.svg)

# **Java: Past, Present and Future**

###### JVM in Details: part 0 of N

Pedro Jordão

---

# Presenter

Pedro Jordão

* Software Developer for 6 years
* C++, Rust and Fortran (:weary:) for fast simulation code
* JVM based languages for backend and GUI
* Currently working with the Foresight team @criticaltechworks with Scala and Apache Flink

---

# Java: A Quick History Overview
(This was totally not copied from wikipedia)

* Initial design in 1991, first release in 1996 by Sun Microsystems
* Meant to be a C++ style language
* Object-Oriented
* Automatically managed memory
* Interpreted*
* Write once run everywhere
* Acquired by Oracle in 2009-10

---

# The Past (1996 - 2014) 
### Frist Release: 1.0 & 1.1 (1996 & 1997)

* No Generics
* No Reflection
* No beans
* No JIT
* No inner classes
* No Collections framework

---

# The Past (1996 - 2014)

### 1.2 (1998)

* Introduced inner classes (which to this day is still a hack in the JVM point of view)
* JIT compiler (only for Windows)

### 1.3 (2000)

* General JIT compilation through HotSpot

---

# The Past (1996 - 2014)

### 1.4 (2002)

* asserts
* regex
* Non-blocking I/O (NIO)
* Logging API

---

# The Past (1996 - 2014)

### 5 (2004)

* Generics 
* Autoboxing
* Enums
* Varargs
* for each syntax

---

# The Past (1996 - 2017)

### 6 & 7 (2006 & 2011)

* Mostly performance updates, low level APIs and security

### 8 (2014)

* Lambda expressions and closures (sort of)
* Default methods 
* New Date & Time APIs (Thank god!)

---

# A Look Back

* Compromises thanks to backwards compatibility 
    * Generics
        * Still better/easier than C++ style templates 
        * No primitives allowed
        * Silly variance annotations (wildcards and PECS rules)
        ```Java
        // Java
        interface Collection<E> ... {
            void addAll(Collection<? extends E> items);
        }
        ```

---

# A Look Back

* Compromises thanks to backwards compatibility 
    * Lambdas are really weird for library writers
    * No proper "functional" types, only single-method interfaces
        * But allowed old code that used single-method interfaces
        to work seamlessly with lambdas without being recompiled

---

# A Look Back

* Compromises thanks to backwards compatibility 
    * Null 
        * F*ck nulls
        * A solved problem by other languages at the time (Haskell, Erlang, ML)
        * But hindsight is 20/20

---

# The Present (2017 - ?)

* New release model from 9 onwards 
    * Releases every 6 months
        * Features are late-bound to a release, when they are ready
        * Small features doesn't block bigger features
        * _Preview Features_
    * New licensing model from Oracle (so just use OpenJDK or one of its forks if you can)


---

# The Present (2017 - ?)

### 9

* Modules + JLinker
* JShell
* AOT
* Reactive Streams

---

# The Present (2017 - ?)

### 10

* New default GC (G1)
* Local variable type inference :pray:
```java
var thingsList = new ArrayList<Things>();
// is better than
ArrayList<Things> thingsList = new ArrayList<>();
```
* This doesn't make Java any less strongly typed!

---

# The Present (2017 - ?)

### 10

* Not just syntactic sugar!

```java
var nums = List.of(1, 2, "three");
// List<? extends Serializable & Comparable<...>>

// Handles captures
var c  = this.getClass();
// Class<cap<?extends ThisClass>>
```

---

# The Present (2017 - ?)
### 11

* Epsilon and ZGC GC
* HTTP client based on Reactive streams
* JavaFX, JavaEE and CORBA are gone

---

# The Present (2017 - ?)

### 12 

* Yet another GC (Shenandoah)
* Switch experessions (Preview, standard on 14)

```java
// Awful example
boolean isWeekend = switch (day) {
    case MONDAY, TUESDAY, THURSDAY, WEDNESDAY, FRIDAY -> false;
    case SATUDARY, SUNDAY -> true;
}
```

---

# The Present (2017 - ?)

### 13

* Text Blocks (Preview)
```java
var json = """
           {
               "companies": [ 
                   "Critical Techworks",
                   "BMW"
                ]
           }
           """
```
---

# The future

### Java 14

* Records!!!! (Preview)
    * a.k.a. Product Types :nerd_face:
    * No more boiler plate for simple data classes
    ```java
    record Point(int x, int y) {
        // Get a reasonable toString, hashcode, equals and getters for free
        // Also, fields are final
    }
    ``` 
---

# The future

### Java 14
* Pattern Matching for instanceof (Preview )
```java
if (obj instanceof String) {
    String s = (String) obj
    // Use s
}
//becomes
if (obj instanceof String s) {
    // use s
}
```
--- 

# The future

### Project Amber

* This is the project that brought us local variable type inference (JDK 10), switch expressions (JDK 12), text blocks (JDK 13) and records (JDK 14)

--- 

# The future

### Project Amber

* Sealed interfaces
    * Sum types! :nerd_face:
```java
sealed interface Shape {}
record Circle(double radius, double center) implements Shape {}
record Square(double side) implements Shape {}
```

--- 

# The future

### Project Amber

* More pattern Matching
```java
void printer(Shape shape) {
    switch (shape) { // no need for a default since sealed types knows all possible values
        case Square s -> println("square");
        case Circle c -> println("a perfect circle");
    }
}
```
--- 

# The future

### Project Amber

* Even more pattern Matching!
```java
    area = switch (shape) { // We can also destructure records
        case Square(var s) -> s * s;
        case Circle(var r, var c) -> PI * r * r;
    }
```

--- 

# The future

### Project Valhalla
* Everything* is a reference currently
```java
final class Point {
    final int x;
    final int y;
}
Point[] pts = ...
```

--- 

# The future

### Project Valhalla
* What we want
    * Nice and flat
    ![](assets/flat.png)

--- 

# The future

### Project Valhalla
* What we get
    * A _lot_ of indirections
    ![](assets/pointers.png)
--- 

# The future

### Project Valhalla
* A reboot of the layout of data in memory
* Everything* is a reference currently
    * All objects have identity
        * ... But not all _need_ identity
        * Everyone pays for that
--- 

# The future

### Project Valhalla

* Mr. Compiler, this is just data, feel free to inline it
```java
inline class Point {
    int x;
    int y;
}
```
--- 

# The future

### Project valhalla

* No more pointer identity
* Compare by state
* Not mutable
* Not nullable *
* Can be flattened inside other data
* Behaves like primitives
--- 

# The future

### Project valhalla

* Unlike primitives:
    * Can have methods, implement interfaces, use encapsulation and be generic
* Great for value-wrappers like `Optional`
* Great for DDD style type declaration
* Tuples!
--- 

# Other Cool Things 

### Project Loom
* Fibers to the Java platform
    * A lightweight thread scheduled by the JVM (not the OS)
    * Low footprint 
    * Small task-switching overhead
    * Easy to write

--- 

# Other Cool Things

### Project Panama
* JNI is not fast!
    * ... But sometimes it is still faster than a java implementation
* JNI is really complex and verbose
* JNI has to do a lot of heavy lifting 
    * Mapping primitives
    * Taking care of handlers for references

--- 

# Other Cool Things

### Project Panama
* Better native code interop
    * FFI without native code!
    * Just annotations about signatures and layout directly on your java code
    * Jextract tool to automate the generation of annotated FFI classes from headers

---

# Social

### Java Yammer

Talk about Java!

### Rust Yammer

Talk about Rust! (And maybe create a JVM?)

---

# Questions?
